# Theme 02 - Web Interfaces
Web UI for Time Manager app

## Development environment
To start dev localy, use the following command :
```
docker-compose up --build
```
## Documentation
To start dev localy, use the following command :
```
npm install -g apidoc
apidoc -i src
```
doc is then in doc/index.html, that you can open in your browser

## Mobile App

To run mobile app localy, begin to download  [JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html  "Download JDK 1.8") :

### Add export global vars on your system

In [Global var](https://www.computerhope.com/issues/ch000549.htm "Define global var") |--> for Windows
In ~/.bash_profile |--> for Mac
In ~/.bashrc |--> for Linux

```
export ANDROID_SDK_ROOT=/Users/[your user]/Library/Android/sdk
export ANDROID_HOME={ANDROID_SDK_ROOT}
```

### Add vars in the "PATH" of your system

```
$ANDROID_SDK_ROOT/emulator:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/tools/bin:$ANDROID_SDK_ROOT/platform-tools
```
### Build app
In VueJS folder execute:
```
npm run build
```

### Run app, go in VueJS App folder then in "cordova" folder

```
cordova run [android or ios] --device |--> for boot on a device
cordova emulate [android or ios] |--> for boot on a emulator
```
---

**Warning**
If you execute ` cordova emulate [android or ios]` you must have created a virutal device in your IDE (ex: Android Studio), doc -->  [Virtual device](https://developer.android.com/studio/run/managing-avds "Add a android virtual device")

---