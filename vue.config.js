// vue.config.js
const webpack = require("webpack");
module.exports = {
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery'
            })
        ]    
    },
    outputDir: 'cordova/www',
    publicPath: './',
    devServer: {
        proxy: {
            '^/api': {
                target: 'http://localhost:4000',
                ws: false,
                changeOrigin: true
            }
        }
    }
}
