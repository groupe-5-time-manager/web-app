import axios from "axios";
import { store } from "../_store";

export function storeUser(response) {
  if (response.data.xsrfToken) {
    localStorage.setItem('user', JSON.stringify(response.data));
    return true;
  }
  return false
}

export function setDefaultHeaders() {
  const user = JSON.parse(localStorage.getItem('user'))
  if (user) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = user.xsrfToken
  }
}

export function setAxiosInterceptors() {
  axios.interceptors.response.use(fulfilled => fulfilled, error => {
    if (error.response.status === 401)
      store.dispatch('auth/logout')
    return Promise.reject(error)
  })
}
