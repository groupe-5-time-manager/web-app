import Vue from 'vue'
import Router from 'vue-router'

import Dashboard    from './views/Dashboard'
import WorkingTimes from './views/WorkingTimes'
import People       from './views/People'
import Clock        from './views/Clock'
import Profile      from './views/Profile'
import UserLogin    from './views/UserLogin'
import UserRegister from './views/UserRegister'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/',                name: 'Dashboard',    component: Dashboard },
    { path: '/working-times',   name: 'WorkingTimes', component: WorkingTimes },
    { path: '/people',          name: 'People',       component: People, },
    { path: '/people/:id',      name: 'Details',      component: Dashboard },
    { path: '/clock',           name: 'Clock',        component: Clock },
    { path: '/profile',         name: 'Profile',      component: Profile },
    { path: '/login',           name: 'Login',        component: UserLogin },
    { path: '/register',        name: 'Register',     component: UserRegister },
  ]
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = localStorage.getItem('user')

  // redirect to login if not logged and try ro access public page
  if (authRequired && !loggedIn) {
    return next('/login')
  }

  // redirect to root if logged and trying to access login or register page
  if (publicPages.includes(to.path) && loggedIn) {
    return next('/')
  }

  // no redirection
  next();
})
