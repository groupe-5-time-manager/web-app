import { userService } from "../_services"

export const user = {
  namespaced: true,
  state: {
    loggedInUser: {
      teams: []
    },
    editProfile: false,
    user: {},
    users: []
  },
  actions: {
    getLoggedInUser({ commit, dispatch }, {userId}) {
      userService
        .getUser(userId)
        .then(
            user => {
              commit('UPDATE_LOGGED_IN_USER', user)
              if (user.roleId === 2)
                dispatch('team/getTeamById', {teamId: user.team.id}, { root: true })
            },
            error => {
              const msg = error.response
                  ? error.response.data.message
                  : `Oops ! There is an error : ${error.message}`
              commit('UPDATE_LOGGED_IN_USER_FAILURE')
              dispatch('alert/error', msg, { root: true })
            }
        )
    },
    updateLoggedInUser({ commit, dispatch }, user) {
      userService
        .updateUser(user)
        .then(
            user => {
              commit('UPDATE_LOGGED_IN_USER', user)
              dispatch('alert/clear', user, { root: true })
            },
            error => {
              const msg = error.response
                  ? error.response.data.message
                  : `Oops ! There is an error : ${error.message}`
              commit('UPDATE_LOGGED_IN_USER_FAILURE')
              dispatch('alert/error', msg, { root: true })
            }
        );
    },
    setEditProfile({ commit }) {
      commit('UPDATE_EDIT_PROFILE')
    },
    getAllUsers({ commit }) {
      userService
        .getAllUsers()
        .then(response => commit('UPDATE_USERS', response.data))
    },
    createUser({ commit, dispatch }, {user, addToTeam, teamId}) {
      userService
        .createUser(user)
        .then(res => {
          commit('UPDATE_USER', res.data)
          if (addToTeam)
            dispatch('team/addTeamUser', { teamId, userId: res.data.id }, {root:true})
        })
    }
  },
  getters: {
    getLoggedInUser: state => state.loggedInUser,
    isEditProfile: state => state.editProfile,
    getUser: state => state.user,
    getUsers: state => state.users,
  },
  mutations: {
    UPDATE_LOGGED_IN_USER(state, user) {
      state.loggedInUser = user
    },
    UPDATE_LOGGED_IN_USER_FAILURE(state) {
      state.loggedInUser = null
    },
    // UPDATE_USER_SUCCESS(state, user) {
    //   state.user = user
    // },
    // USER_UPDATE_FAILURE(state) {
    //   state.user = null
    // },
    UPDATE_EDIT_PROFILE(state) {
      state.editProfile = !state.editProfile
    },
    UPDATE_USER(state, user) {
      state.user = user
    },
    UPDATE_USERS(state, users) {
      state.users = users
    }
  }
};
