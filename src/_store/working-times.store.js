import { workingTimesService } from "../_services"

export const workingTimes = {

  namespaced: true,

  state: {
    workingTime: { id: null, start: null, end: null },
    workingTimes: [],
    dateLocale: 'en-GB',
    dateOptions: {
      day: 'numeric',
      month: 'short',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    },
    query: {
      userId: undefined,
      start: undefined,
      end: undefined
    }
  },

  getters: {
    getWorkingTime: state => state.workingTime,
    getWorkingTimes: state => state.workingTimes,
    getDateLocale: state => state.dateLocale,
    getDateOptions: state => state.dateOptions,
    getQuery: state => state.query
  },

  actions: {
    getWorkingTimesByRange({ commit }, { userId, data }) {
      console.log("{ userId, data }", { userId, data })
        workingTimesService
          .getWorkingTimesByRange({ userId, data })
          .then(res => commit('UPDATE_WORKING_TIMES', res.data))
    },
    getWorkingTimeById() {},
    getWorkingTimes({ commit }, { userId }) {
      workingTimesService
        .getWorkingTimes({ userId })
        .then(res => commit('UPDATE_WORKING_TIMES', res.data))
    },
    createWorkingTime({ commit }, { userId, data }) {
      workingTimesService
        .createWorkingTime({ userId, data })
        .then(res => commit('UPDATE_WORKING_TIME', res.data))
    },
    updateWorkingTime({ commit }, { wt }) {
      workingTimesService
        .updateWorkingTime({ wt })
        .then(res => commit('UPDATE_WORKING_TIME', res.data))
    },
    deleteWorkingTime({ commit }, { wtId }) {
      workingTimesService
        .deleteWorkingTime({ wtId })
        .then(res => commit('UPDATE_WORKING_TIMES', res.data))
    },
  },

  mutations: {
    UPDATE_WORKING_TIME(state, data) {
      state.workingtime = data
    },
    UPDATE_WORKING_TIMES(state, data) {
      state.workingTimes = data
    }
  }

}
