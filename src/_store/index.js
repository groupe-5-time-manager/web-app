import Vue from 'vue';
import Vuex from 'vuex';

import { auth }   from "./auth.store";
import { clock }  from "./clock.store";
import { alert }  from "./alert.store";
import { user }   from "./user.store";
import { team }   from "./team.store";
import { workingTimes } from "./working-times.store";

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    'auth': auth,
    'clock': clock,
    'alert': alert,
    'user': user,
    'team': team,
    'workingTimes': workingTimes
  }
});
