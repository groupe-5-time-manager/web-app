import { router } from "../router";
import { authService } from '../_services';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
    ? { loggedIn: true, user: user }
    : { loggedIn: false, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ dispatch, commit }, credentials) {
      authService
        .login(credentials)
        .then(
          response => {
            localStorage.setItem('user', JSON.stringify(response.data))
            commit('SUCCESS', response.data);
            dispatch('alert/clear', response, { root: true });
            dispatch('user/getLoggedInUser', response.data.userId, { root: true });
            router.push({ name: 'Profile' });
          },
          error => {
            const msg = error.response
                ? error.response.data.message
                : `Oops ! There is an error : ${error.message}`;
            commit('FAILURE');
            console.log("msg", msg)
            dispatch('alert/error', msg, { root: true })
          })
    },
    register({ dispatch, commit }, user) {
      authService
        .register(user)
        .then(
          response => {
            localStorage.setItem('user', JSON.stringify(response.data))
            commit('SUCCESS', response.data);
            dispatch('alert/clear', response, { root: true });
            dispatch('user/getLoggedInUser', response.data.userId, { root: true });
            router.push({ name: 'Profile' });
          },
          error => {
            const msg = error.response
                ? error.response.data.message
                : `Oops ! There is an error : ${error.message}`;
            commit('FAILURE');
            dispatch('alert/error', msg, { root: true })
          })
    },
    logout({ commit }) {
      authService
        .logout()
        .then(() => {
          commit('LOGOUT');
          router.push({ name: 'Login' });
        });
    }
  },
  getters: {
    isLoggedIn: state => state.loggedIn,
    getUser: state => state.user
  },
  mutations: {
    SUCCESS(state, user) {
      state.loggedIn = true;
      state.user = user;
    },
    FAILURE(state) {
      state.loggedIn = false;
      state.user = null;
    },
    LOGOUT(state) {
      state.loggedIn = false;
      state.user = null;
    },
  }
};
