import axios from 'axios'

export const clock = {
    state: {
        clock: {
            time: '',
            status: false
        },
        clocks: []
    },
    getters: {
        clock: state => state.clock,
        clocks: state => state.clocks
    }, 
    mutations: {
        UPDATE_CLOCK: (state, data) => {
            state.clock.time = data.time
            state.clock.status = data.status
        },
        UPDATE_CLOCKS: (state, data) => {
            state.clocks = data.clocks
        }
    },
    actions: {
        updateClock: async (store, data) => {
            try {
                store.commit('UPDATE_CLOCK', {time: data.time, status: data.status})
                await axios.post(`/api/clocks/${data.id}`, {
                    time: data.time,
                    status: data.status
                })
            } catch(error) {
                console.log(error);
            }
        },
        updateClocks: async (store, {id}) => {
            try {   
                const response = await axios.get(`/api/clocks/${id}`)
                let data = response.data
                data.sort(function(a, b){
                    return b.time-a.time
                })
                if(store.getters.clock.time == "" && data.length > 0) {
                    let clockItem = data[0]
                    store.commit('UPDATE_CLOCK', {time: clockItem.time, status: clockItem.status})
                }
                data.forEach(item => {
                    item.time = new Date(item.time * 1000).toLocaleDateString('en-GB', {day : 'numeric', month : 'short', year : 'numeric', hour: 'numeric', minute: 'numeric'})
                    item.status = item.status ? 'Clock in' : 'Clock out'
                });
                store.commit('UPDATE_CLOCKS', { clocks: data })
            } catch(error) {
                console.log(error);
            }
        }
    }
}
