export const alert = {
  namespaced: true,
  state: {
    type: null,
    message: null
  },
  actions: {
    success({ commit }, message) {
      commit('SUCCESS', message);
    },
    warning({ commit }, message) {
      commit('WARNING', message)
    },
    error({ commit }, message) {
      commit('ERROR', message);
    },
    clear({ commit }) {
      commit('CLEAR');
    }
  },
  getters: {
    getType: state => state.type,
    getMessage: state => state.message
  },
  mutations: {
    SUCCESS(state, message) {
      state.type = 'success'
      state.message = message
    },
    WARNING(state, message) {
      state.type = 'warning'
      state.message = message
    },
    ERROR(state, message) {
      state.type = 'error'
      state.message = message
    },
    CLEAR(state) {
      state.type = null
      state.message = null
    }
  }
}
