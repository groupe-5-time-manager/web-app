import { teamService } from "../_services"

export const team = {
  namespaced: true,
  state: {
    team: {},
    users: [],
    teams: [],
    loading: false,
  },
  actions: {
    createTeam({ commit }, team) {
      teamService
        .createTeam(team)
        .then(res => console.log("res =>", res),)
    },
    getAllTeams() {
      teamService
          .getAllTeams()
          .then(res => console.log("res =>", res))
    },
    getTeamById({ commit, dispatch }, { teamId }) {
      commit('SET_LOADING')
      teamService
        .getTeamById(teamId)
        .then(res => {
          commit('UPDATE_TEAM', res.data)
          commit('SET_LOADING')
          dispatch('team/getTeamUsers', res.data.id, {root: true})
        })
    },
    updateTeam({ commit }, { teamId, teamName }) {
      teamService
        .updateTeam(teamId, teamName)
        .then(res => console.log("res =>", res))
    },
    deleteTeam({ commit }, teamId) {
      teamService
        .deleteTeam(teamId)
        .then(res => console.log("res =>", res))
    },
    getTeamUsers({ commit }, teamId) {
      teamService
        .getTeamUsers(teamId)
        .then(res => commit('UPDATE_USERS', res.data))
    },
    addTeamUser({ commit }, {teamId, userId}) {
      teamService
        .addTeamUser(teamId, userId)
        .then(res => commit('UPDATE_USERS', res.data))
    },
    removeTeamUser({ commit }, teamId, userId) {
      teamService
        .removeTeamUser(teamId, userId)
        .then(res => console.log("res =>", res))
    }
  },
  getters: {
    getTeam: state => state.team,
    getUsers: state => state.users,
    getTeams: state => state.teams,
    isLoading: state => state.loading
  },
  mutations: {
    UPDATE_TEAM(state, team) {
      state.team = team
    },
    UPDATE_TEAMS(state, teams) {
      state.teams = teams
    },
    DELETE_TEAM(state) {
      state.team = {}
    },
    DELETE_TEAMS(state) {
      state.teams = []
    },
    SET_LOADING(state) {
      state.loading = !state.loading
    },
    UPDATE_USERS(state, users) {
      state.users = users
    }
  }
};
