// VUE JS
import Vue from 'vue'
import { store } from './_store'
import { router } from './router'
import { setDefaultHeaders, setAxiosInterceptors } from './_helpers'
import App from './App'

// VUETIFY
import vuetify from './plugins/vuetify'
import '@mdi/font/css/materialdesignicons.css'

// CUSTOM CSS
import '@/assets/scss/main.scss'

// VUE MORRIS
import Raphael from 'raphael/raphael'
global.Raphael = Raphael

// JQUERY
global.jQuery = require('jquery')
let $ = global.jQuery
window.$ = $

// VUE.JS CONF
Vue.config.productionTip = false

// AXIOS + HEADERS
setDefaultHeaders()
setAxiosInterceptors()

new Vue({
  el: '#app',
  router,
  store,
  vuetify,
  render: h => h(App)
})
