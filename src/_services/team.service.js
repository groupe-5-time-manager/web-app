import axios from 'axios';

export const teamService = {
  createTeam,
  getAllTeams,
  getTeamById,
  updateTeam,
  deleteTeam,
  getTeamUsers,
  addTeamUser,
  removeTeamUser,
};

async function createTeam({ name, managerId, userList }) {
  try {
    const config = {
      headers: { 'Content-Type': 'application/json' }
    }
    const team = {
      name,
      managerId,
      userList
    }
    const response = await axios.post(`/api/teams`, team, config)

    return response.data
  } catch (e) {
    console.error(`Error createTeam()`, e.response)
    return e.response
  }
}

async function getAllTeams() {
  try {
    const response = await axios.get(`/api/teams`)

    return response.data
  } catch (e) {
    console.error(`Error getAllTeam()`, e.response)
    return e.response
  }
}

async function getTeamById(teamId) {
  try {
    const response = await axios.get(`/api/teams/${teamId}`)
    return response
  } catch (e) {
    throw new Error(e)
  }
}

async function updateTeam(teamId, { name }) {
  try {
    const config = {
      headers: { 'Content-Type': 'application/json' }
    }
    const response = await axios.put(`/api/teams/${teamId}`, { name }, config)
    return response.data
  } catch (e) {
    throw new Error(e)
  }
}

async function deleteTeam(teamId) {
  try {
    const response = await axios.delete(`/api/teams/${teamId}`)

    return response.data
  } catch (e) {
    console.error(`Error deleteTeam()`, e.response);
    return e.response
  }
}

async function getTeamUsers(teamId) {
  try {
    return await axios.get(`/api/teams/${teamId}/users`)
  } catch (e) {
    throw new Error(e)
  }
}

async function addTeamUser(teamId, userId) {
  try {
    const config = {
      headers: { 'Content-Type': 'application/json' }
    }
    const data = {
      users: [userId]
    }
    const response = await axios.post(`/api/teams/${teamId}/users`, data, config)
    return response
  } catch (e) {
    throw new Error(e)
  }
}

async function removeTeamUser(teamId, userId) {
  try {
    const response = await axios.delete(`/api/teams/${teamId}/users/${userId}`)

    return response.data
  } catch (e) {
    console.error(`Error removeTeamUser()`, e.response);
    return e.response
  }
}
