import axios from 'axios';
import { storeUser } from '../_helpers'
import { setDefaultHeaders } from '../_helpers'

export const authService = {
  login,
  register,
  logout,
};

function login(credentials) {
  const config = {
    headers: { 'Content-Type': 'application/json' }
  }

  return new Promise((resolve, reject) => {
    axios.post('api/auth/signin', credentials, config)
      .then(
        response => {
          if (storeUser(response)) {
            setDefaultHeaders()
            resolve(response)
          }
        },
        error => {
          console.error("Login request error =>", error);
          localStorage.removeItem('user');
          reject(error);
        })

  })
}

function register(user) {
  const options = {
    headers: { 'Content-Type': 'application/json' }
  }

  return new Promise((resolve, reject) => {
    axios.post('/api/auth/signup', user, options)
      .then(
        response => {
          if (storeUser(response)) {
            setDefaultHeaders()
            resolve(response);
          }
        },
        error => {
          console.error("Register request error =>", error);
          localStorage.removeItem('user');
          reject(error);
        })
  })
}

function logout() {
  return new Promise((resolve) => {
    localStorage.removeItem('user');
    resolve();
  })
}
