import axios from 'axios';

export const userService = {
  getUser,
  getAllUsers,
  createUser,
  updateUser
};

function getUser(userId) {
  return new Promise((resolve, reject) => {
    axios
      .get(`/api/users/${userId}`)
      .then(
        response => resolve(response.data),
        error => reject(error)
      )
  })
}

async function getAllUsers() {
  try {
    const response = axios.get(`/api/users`)
    return response
  } catch (e) {
    throw new Error(e)
  }
}

async function createUser(user) {
  try {
    const config = {
      headers: { 'Content-Type': 'application/json' }
    }
    const response = await axios.post(`/api/users`, user, config)
    return response
  } catch (e) {
    throw new Error(e)
  }
}

function updateUser(user) {
  const config = {
    headers: { 'Content-Type': 'application/json' }
  }
  return new Promise((resolve, reject) => {
    axios
      .put(`/api/users/${user.id}`, user, config)
      .then(
        response => resolve(response.data),
        error => reject(error)
      )
  })
}
