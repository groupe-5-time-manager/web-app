import axios from "axios"

export const workingTimesService = {
  getWorkingTimes,
  getWorkingTimesByRange,
  createWorkingTime,
  updateWorkingTime,
  deleteWorkingTime
}

async function getWorkingTimes({ userId }) {
  try {
    return await axios.get(`/api/workingtimes/${userId}?start=&end=`)
  } catch (e) {
    throw new Error(e)
  }
}

async function getWorkingTimesByRange({ userId, data}) {
  try {
    return await axios.get(`/api/workingtimes/${userId}?start=${data.start}&end=${data.end}`)
  } catch (e) {
    throw new Error(e)
  }
}

async function createWorkingTime({ userId, data }) {
  const config = { headers: { 'Content-Type': 'application/json' } }
  const body = { start: data.start, end: data.end }

  try {
    return await axios.post(`/api/workingtimes/${userId}`, body, config)
  } catch (e) {
    throw new Error(e)
  }
}

async function updateWorkingTime({ wt }) {
  try {
    const data = { start: wt.start, end: wt.end }

    return await axios.put(`/api/workingtimes/${wt.id}`, data)
  } catch (e) {
    throw new Error(e)
  }
}

async function deleteWorkingTime({ wtId }) {
  try {
    return await axios.delete(`/api/workingtimes/${wtId}`)
  } catch (e) {
    throw new Error(e)
  }
}
